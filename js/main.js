let hints = document.getElementsByClassName("hint");

const blockHint = (hint, input) => {
  if (hint.classList.contains("used")) {
    return false;
  } else {
    hint.classList.add("used");
    input.value = "1";
    return true;
  }
};

const closeModal = (modal) => {
  modal.classList.remove("show");
};

const showModal = (modal) => {
  modal.classList.add("show");
}


const showRegisterModal = (modal) => {
  closeModal(modal);
  showModal(document.getElementById("modalRegister"));
}

document.getElementById("hintHall").addEventListener(
  "click",
  function() {
    let dataInput = document.querySelector('[name="hint-hall"]');
    if (blockHint(this, dataInput)) {
      let answers = dataInput.dataset.answer.split(", ");
      let hintsBlocks = document.getElementsByClassName("hall-answer");

      for (let i = 0; i < answers.length; i++) {
        hintsBlocks[i].style.height = answers[i] * 2 + "px";
      }
      document.getElementById("modalHallAnswer").classList.add("show");
      console.log(answers);
    } else {
      return;
    }
  },
  null
);

document.getElementById("hintFifty").addEventListener("click", function() {
  let dataInput = document.querySelector('[name="hint-fifty"]');
  if (blockHint(this, dataInput)) {
    let answers = dataInput.dataset.answer.split(", ");
    console.log(answers);
    answers.forEach(element => {
      document
        .querySelector(`button[value=${element}]`)
        .classList.add("transparent");
      document.querySelector(`button[value=${element}]`).disabled = true;
    });
  } else {
    return;
  }
});

document.getElementById("hintCall").addEventListener("click", function() {
  let dataInput = document.querySelector('[name="hint-call"]');
  if (blockHint(this, dataInput)) {
    document.getElementById("modalCall").classList.add("show");   
  } else {
    return;
  }
});


const showAnswer = () => {
  let rightAnswer = document.getElementById("rightAnswer").value;
  setTimeout(function() {
    document
      .querySelector(`button[value=${rightAnswer}]`)
      .classList.add("right-answer");
  }, 1000);
};

[].forEach.call(document.getElementsByClassName("answer"), function(el) {
  el.addEventListener(
    "click",
    (e) => {
      e.preventDefault();
      el.classList.add("chosen-answer");    
      showAnswer();
      for (let i=0; i<document.getElementsByClassName("answer").length; i++){
        document.getElementsByClassName("answer")[i].disabled = true
      } 
      setTimeout(function() {
        el.classList.remove("chosen-answer");
        document.getElementById("questionsForm").submit(); 
      }, 4000);
    },
    null
  );
});

[].forEach.call(document.getElementsByClassName("modal"), function(el) {
  el.addEventListener("click", (e) => {
    if(e.target.classList.contains("modal-body")){
      return;
    }    
    if(e.target.classList.contains("modal-close") || e.target.classList.contains("modal-btn") || e.target.classList.contains("modal")){
      closeModal(el); 
    }
  });
});


